const components = {
    schemas: {
      Error: {
        /** @description Error code */
        code?: number,
        /** @description Errors */
        errors?: Record<string, never>;
        /** @description Error message */
        message?: string;
        /** @description Error name */
        status?: string;
      };
      PaginationMetadata: {
        total?: number;
        total_pages?: number;
        first_page?: number;
        last_page?: number;
        page?: number;
        previous_page?: number;
        next_page?: number;
      };
      HealthCheck: {
        message: string;
      };
      ProgramError: {
        code?: number;
        errors?: Record<string, never>;
        message?: string;
        status?: string;
      };
      ProgramList: {
        id: number;
        name: string;
        /** Format: date-time */
        created_at: string;
        /** @enum {string} */
        status: 'DRAFT' | 'ACTIVE' | 'PUBLISHED' | 'ARCHIVED';
        /** Format: date-time */
        start_date: string;
        /** @enum {string} */
        program_type: 'GENERIC' | 'DYNAMIC_OPERATING_ENVELOPES' | 'DEMAND_MANAGEMENT';
        /** Format: date-time */
        end_date: string;
      };
      PaginatedProgramsList: {
        pagination_start: number;
        pagination_end: number;
        count: number;
        results?: components['schemas']['ProgramList'][];
      };
      ServiceWindow: {
        fri: boolean;
        sat: boolean;
        tue: boolean;
        start_hour: number;
        sun: boolean;
        end_hour: number;
        mon: boolean;
        wed: boolean;
        thu: boolean;
      };
      DemandManagementDispatchConstraints: {
        max_total_energy_per_timeperiod?: number;
        /** @enum {string} */
        timeperiod?: 'DAY' | 'WEEK' | 'MONTH' | 'YEAR' | 'PROGRAM_DURATION';
        /** @enum {string} */
        max_total_energy_unit?: 'KWH' | 'MWH';
      };
      MaxNumberOfEventsPerTimePeriod: {
        YEAR?: number;
        WEEK?: number;
        MONTH?: number;
        DAY?: number;
        PROGRAM_DURATION?: number;
      };
      MinMax: {
        min?: number | null;
        max?: number | null;
      };
      CumulativeEventDuration: {
        YEAR?: components['schemas']['MinMax'];
        WEEK?: components['schemas']['MinMax'];
        MONTH?: components['schemas']['MinMax'];
        DAY?: components['schemas']['MinMax'];
        PROGRAM_DURATION?: components['schemas']['MinMax'];
      };
      DispatchConstraints: {
        max_number_of_events_per_timeperiod?: components['schemas']['MaxNumberOfEventsPerTimePeriod'];
        event_duration_constraint?: components['schemas']['MinMax'];
        cumulative_event_duration?: components['schemas']['CumulativeEventDuration'];
      };
      ResourceEligibilityCriteria: {
        max_real_power_rating?: number;
        min_real_power_rating?: number;
      };
      DispatchNotification: {
        text?: string;
        /** @enum {integer} */
        lead_time: 10 | 30 | 60 | 360 | 1440;
      };
      DispatchOptOut: {
        /** @enum {string} */
        timeperiod: 'DAY' | 'WEEK' | 'MONTH' | 'YEAR' | 'PROGRAM_DURATION';
        value: number;
      };
      DynamicOperatingEnvelopeFields: {
        /** @enum {string} */
        limit_type?: 'REAL_POWER' | 'REACTIVE_POWER';
        /** @enum {string} */
        schedule_time_horizon_timeperiod?: 'MINUTES' | 'HOURS' | 'DAYS';
        /** @enum {string} */
        calculation_frequency?: 'EVERY_5_MINUTES' | 'EVERY_15_MINUTES' | 'EVERY_30_MINUTES' | 'HOURLY' | 'DAILY';
        schedule_time_horizon_number?: number;
        /** @enum {string} */
        control_type?: 'EXPORT_LIMIT_ONLY' | 'EXPORT_AND_IMPORT_LIMIT';
        schedule_timestep_mins?: number;
      };
      AvailableOperatingMonths: {
        mar: boolean;
        apr: boolean;
        aug: boolean;
        jan: boolean;
        feb: boolean;
        jun: boolean;
        nov: boolean;
        dec: boolean;
        sep: boolean;
        oct: boolean;
        may: boolean;
        jul: boolean;
      };
      GeneralFields: {
        name: string;
        /** @enum {string} */
        program_priority?: 'P0' | 'P1' | 'P2' | 'P3' | 'P4' | 'P5';
        /** @enum {string} */
        availability_type?: 'ALWAYS_AVAILABLE' | 'SERVICE_WINDOWS';
        define_contractual_target_capacity?: boolean;
        /** @enum {string} */
        status?: 'DRAFT' | 'ACTIVE' | 'PUBLISHED' | 'ARCHIVED';
        /** Format: date-time */
        start_date?: string;
        /** @enum {string} */
        program_type: 'GENERIC' | 'DYNAMIC_OPERATING_ENVELOPES' | 'DEMAND_MANAGEMENT';
        /** Format: date-time */
        end_date?: string;
        /** @enum {string} */
        notification_type?: 'SMS_EMAIL' | 'CUSTOM_API' | 'OPEN_ADR';
        check_der_eligibility?: boolean;
      };
      CreateUpdateProgram: {
        avail_service_windows?: components['schemas']['ServiceWindow'][];
        demand_management_constraints?: components['schemas']['DemandManagementDispatchConstraints'];
        track_event?: boolean;
        dispatch_constraints?: components['schemas']['DispatchConstraints'];
        resource_eligibility_criteria?: components['schemas']['ResourceEligibilityCriteria'];
        dispatch_notifications?: components['schemas']['DispatchNotification'][];
        dispatch_max_opt_outs?: components['schemas']['DispatchOptOut'][];
        /** @enum {string} */
        dispatch_type?: 'DER_GATEWAY' | 'API' | 'OTHER';
        control_options?: (
          | 'OP_MOD_CONNECT'
          | 'OP_MOD_ENERGIZE'
          | 'OP_MOD_FIXED_PF_ABSORB_W'
          | 'OP_MOD_FIXED_PF_INJECT_W'
          | 'OP_MOD_FIXED_VAR'
          | 'OP_MOD_FIXED_W'
          | 'OP_MOD_FRED_WATT'
          | 'OP_MOD_FREQ_DROOP'
          | 'OP_MOD_HFRT_MAY_TRIP'
          | 'OP_MOD_HFRT_MUST_TRIP'
          | 'OP_MOD_HVRT_MOMENTARY_CESSATION'
          | 'OP_MOD_HVRT_MUST_TRIP'
          | 'OP_MOD_LFRT_MAY_TRIP'
          | 'OP_MOD_LFRT_MUST_TRIP'
          | 'OP_MOD_LVRT_MAY_TRIP'
          | 'OP_MOD_LVRT_MOMENTARY_CESSATION'
          | 'OP_MOD_LVRT_MUST_TRIP'
          | 'OP_MOD_MAX_LIM_W'
          | 'OP_MOD_TARGET_VAR'
          | 'OP_MOD_TARGET_W'
          | 'OP_MOD_VOLT_VAR'
          | 'OP_MOD_VOLT_WATT'
          | 'OP_MOD_WATT_PF'
          | 'OP_MOD_WATT_VAR'
          | 'RAMP_TMS'
        )[];
        dynamic_operating_envelope_fields?: components['schemas']['DynamicOperatingEnvelopeFields'];
        avail_operating_months?: components['schemas']['AvailableOperatingMonths'];
        general_fields: components['schemas']['GeneralFields'];
      };
      HolidayCalendarEventsDict: {
        name?: string;
        category?: string;
        endDate?: string;
        startDate?: string;
        substitutionDate?: string;
      };
      ProgramFull: {
        name: string;
        dispatch_constraints?: components['schemas']['DispatchConstraints'];
        resource_eligibility_criteria?: components['schemas']['ResourceEligibilityCriteria'];
        /** @enum {string} */
        limit_type?: 'REAL_POWER' | 'REACTIVE_POWER';
        /** @enum {string} */
        dispatch_type?: 'DER_GATEWAY' | 'API' | 'OTHER';
        /** @enum {string} */
        control_type?: 'EXPORT_LIMIT_ONLY' | 'EXPORT_AND_IMPORT_LIMIT';
        /** @enum {string} */
        availability_type?: 'ALWAYS_AVAILABLE' | 'SERVICE_WINDOWS';
        avail_operating_months?: components['schemas']['AvailableOperatingMonths'];
        check_der_eligibility?: boolean;
        /** @enum {string} */
        program_priority?: 'P0' | 'P1' | 'P2' | 'P3' | 'P4' | 'P5';
        dispatch_notifications?: components['schemas']['DispatchNotification'][];
        schedule_time_horizon_number?: number;
        holiday_exclusions?: components['schemas']['HolidayCalendarEventsDict'];
        avail_service_windows?: components['schemas']['ServiceWindow'][];
        demand_management_constraints?: components['schemas']['DemandManagementDispatchConstraints'];
        track_event?: boolean;
        /** @enum {string} */
        status?: 'DRAFT' | 'ACTIVE' | 'PUBLISHED' | 'ARCHIVED';
        /** Format: date-time */
        start_date?: string;
        control_options?: (
          | 'OP_MOD_CONNECT'
          | 'OP_MOD_ENERGIZE'
          | 'OP_MOD_FIXED_PF_ABSORB_W'
          | 'OP_MOD_FIXED_PF_INJECT_W'
          | 'OP_MOD_FIXED_VAR'
          | 'OP_MOD_FIXED_W'
          | 'OP_MOD_FRED_WATT'
          | 'OP_MOD_FREQ_DROOP'
          | 'OP_MOD_HFRT_MAY_TRIP'
          | 'OP_MOD_HFRT_MUST_TRIP'
          | 'OP_MOD_HVRT_MOMENTARY_CESSATION'
          | 'OP_MOD_HVRT_MUST_TRIP'
          | 'OP_MOD_LFRT_MAY_TRIP'
          | 'OP_MOD_LFRT_MUST_TRIP'
          | 'OP_MOD_LVRT_MAY_TRIP'
          | 'OP_MOD_LVRT_MOMENTARY_CESSATION'
          | 'OP_MOD_LVRT_MUST_TRIP'
          | 'OP_MOD_MAX_LIM_W'
          | 'OP_MOD_TARGET_VAR'
          | 'OP_MOD_TARGET_W'
          | 'OP_MOD_VOLT_VAR'
          | 'OP_MOD_VOLT_WATT'
          | 'OP_MOD_WATT_PF'
          | 'OP_MOD_WATT_VAR'
          | 'RAMP_TMS'
        )[];
        /** @enum {string} */
        program_type: 'GENERIC' | 'DYNAMIC_OPERATING_ENVELOPES' | 'DEMAND_MANAGEMENT';
        dynamic_operating_envelope_fields?: components['schemas']['DynamicOperatingEnvelopeFields'];
        /** Format: date-time */
        end_date?: string;
        /** @enum {string} */
        notification_type?: 'SMS_EMAIL' | 'CUSTOM_API' | 'OPEN_ADR';
        schedule_timestep_mins?: number;
        id: number;
        define_contractual_target_capacity?: boolean;
        dispatch_max_opt_outs?: components['schemas']['DispatchOptOut'][];
        /** @enum {string} */
        schedule_time_horizon_timeperiod?: 'MINUTES' | 'HOURS' | 'DAYS';
        /** @enum {string} */
        calculation_frequency?: 'EVERY_5_MINUTES' | 'EVERY_15_MINUTES' | 'EVERY_30_MINUTES' | 'HOURLY' | 'DAILY';
      };
      GeneralFieldsUpdate: {
        name?: string;
        /** @enum {string} */
        program_priority?: 'P0' | 'P1' | 'P2' | 'P3' | 'P4' | 'P5';
        /** @enum {string} */
        availability_type?: 'ALWAYS_AVAILABLE' | 'SERVICE_WINDOWS';
        define_contractual_target_capacity?: boolean;
        /** @enum {string} */
        status?: 'DRAFT' | 'ACTIVE' | 'PUBLISHED' | 'ARCHIVED';
        /** Format: date-time */
        start_date?: string;
        /** @enum {string} */
        program_type?: 'GENERIC' | 'DYNAMIC_OPERATING_ENVELOPES' | 'DEMAND_MANAGEMENT';
        /** Format: date-time */
        end_date?: string;
        /** @enum {string} */
        notification_type?: 'SMS_EMAIL' | 'CUSTOM_API' | 'OPEN_ADR';
        check_der_eligibility?: boolean;
      };
      UpdateProgram: {
        avail_service_windows?: components['schemas']['ServiceWindow'][];
        demand_management_constraints?: components['schemas']['DemandManagementDispatchConstraints'];
        track_event?: boolean;
        dispatch_constraints?: components['schemas']['DispatchConstraints'];
        resource_eligibility_criteria?: components['schemas']['ResourceEligibilityCriteria'];
        dispatch_notifications?: components['schemas']['DispatchNotification'][];
        dispatch_max_opt_outs?: components['schemas']['DispatchOptOut'][];
        /** @enum {string} */
        dispatch_type?: 'DER_GATEWAY' | 'API' | 'OTHER';
        control_options?: (
          | 'OP_MOD_CONNECT'
          | 'OP_MOD_ENERGIZE'
          | 'OP_MOD_FIXED_PF_ABSORB_W'
          | 'OP_MOD_FIXED_PF_INJECT_W'
          | 'OP_MOD_FIXED_VAR'
          | 'OP_MOD_FIXED_W'
          | 'OP_MOD_FRED_WATT'
          | 'OP_MOD_FREQ_DROOP'
          | 'OP_MOD_HFRT_MAY_TRIP'
          | 'OP_MOD_HFRT_MUST_TRIP'
          | 'OP_MOD_HVRT_MOMENTARY_CESSATION'
          | 'OP_MOD_HVRT_MUST_TRIP'
          | 'OP_MOD_LFRT_MAY_TRIP'
          | 'OP_MOD_LFRT_MUST_TRIP'
          | 'OP_MOD_LVRT_MAY_TRIP'
          | 'OP_MOD_LVRT_MOMENTARY_CESSATION'
          | 'OP_MOD_LVRT_MUST_TRIP'
          | 'OP_MOD_MAX_LIM_W'
          | 'OP_MOD_TARGET_VAR'
          | 'OP_MOD_TARGET_W'
          | 'OP_MOD_VOLT_VAR'
          | 'OP_MOD_VOLT_WATT'
          | 'OP_MOD_WATT_PF'
          | 'OP_MOD_WATT_VAR'
          | 'RAMP_TMS'
        )[];
        dynamic_operating_envelope_fields?: components['schemas']['DynamicOperatingEnvelopeFields'];
        avail_operating_months?: components['schemas']['AvailableOperatingMonths'];
        general_fields?: components['schemas']['GeneralFieldsUpdate'];
      };
      JSONFile: {
        /** Format: binary */
        file?: string;
      };
      AvailableDersResponse: {
        name?: string;
        der_id: string;
        /** @enum {string} */
        nameplate_rating_unit?:
          | 'kW'
          | 'kVAr'
          | 'MW'
          | 'MVAr'
          | 'V'
          | 'A'
          | 'PF'
          | 'kWh'
          | 'MWh'
          | 'kVArh'
          | 'MVArh'
          | 'load_factor'
          | 'F';
        /** @enum {string} */
        der_type?: 'DR' | 'BESS' | 'EV_CHRG_STN' | 'PV' | 'WIND_FARM' | 'SYNC_GEN';
        /** @enum {string} */
        resource_category?: 'CNI' | 'GENERIC' | 'RES' | 'UTIL' | 'VPP';
        service_provider_id?: number;
        nameplate_rating?: number;
      };
      NotificationContact: {
        /** Format: email */
        email_address?: string;
        phone_number?: string;
      };
      Address: {
        city?: string;
        state?: string;
        apt_unit?: string;
        country?: string;
        street?: string;
        zip_code?: string;
      };
      AssociatedDers: {
        id: number;
        name?: string;
        der_id: string;
        /** @enum {string} */
        nameplate_rating_unit?:
          | 'kW'
          | 'kVAr'
          | 'MW'
          | 'MVAr'
          | 'V'
          | 'A'
          | 'PF'
          | 'kWh'
          | 'MWh'
          | 'kVArh'
          | 'MVArh'
          | 'load_factor'
          | 'F';
        /** @enum {string} */
        der_type?: 'DR' | 'BESS' | 'EV_CHRG_STN' | 'PV' | 'WIND_FARM' | 'SYNC_GEN';
        /** @enum {string} */
        resource_category?: 'CNI' | 'GENERIC' | 'RES' | 'UTIL' | 'VPP';
        nameplate_rating?: number;
      };
      PrimaryContact: {
        /** Format: email */
        email_address: string;
        phone_number: string;
      };
      ServiceProvider: {
        id: number;
        /** @enum {string} */
        service_provider_type: 'AGGREGATOR' | 'C_AND_I' | 'RESIDENTIAL';
        name: string;
        notification_contact?: components['schemas']['NotificationContact'];
        /** @enum {string} */
        status: 'ACTIVE' | 'INACTIVE';
        address?: components['schemas']['Address'];
        ders?: components['schemas']['AssociatedDers'][];
        primary_contact: components['schemas']['PrimaryContact'];
      };
      GeneralFieldsServiceProvider: {
        name: string;
        /** @enum {string} */
        service_provider_type: 'AGGREGATOR' | 'C_AND_I' | 'RESIDENTIAL';
        /** @enum {string} */
        status: 'ACTIVE' | 'INACTIVE';
      };
      CreateUpdateServiceProvider: {
        notification_contact?: components['schemas']['NotificationContact'];
        general_fields: components['schemas']['GeneralFieldsServiceProvider'];
        primary_contact: components['schemas']['PrimaryContact'];
        address?: components['schemas']['Address'];
      };
      ServiceProviderError: {
        code?: number;
        errors?: Record<string, never>;
        message?: string;
        status?: string;
      };
      ServiceProviderUpload: {
        /** Format: byte */
        file: string;
      };
      ServiceProviderDerAssociationUpload: {
        /** Format: byte */
        file: string;
      };
      EnrollmentError: {
        code?: number;
        errors?: Record<string, never>;
        message?: string;
        status?: string;
      };
      DemandResponse: {
        export_target_capacity?: number;
        import_target_capacity?: number;
      };
      DynamicOperatingEnvelopes: {
        default_limits_active_power_import_kw?: number;
        default_limits_active_power_export_kw?: number;
        default_limits_reactive_power_import_kw?: number;
        default_limits_reactive_power_export_kw?: number;
      };
      Enrollment: {
        id: number;
        program_id: number;
        der_id: string;
        /** @enum {string} */
        enrollment_status: 'ACCEPTED' | 'REJECTED' | 'PENDING';
        demand_response?: components['schemas']['DemandResponse'];
        service_provider_id: number;
        /** @enum {string} */
        rejection_reason?:
          | 'DER_NOT_FOUND'
          | 'ELIGIBILITY_DATA_NOT_FOUND'
          | 'DER_DOES_NOT_MEET_CRITERIA'
          | 'DER_NOT_ASSOCIATED_WITH_SERVICE_PROVIDER';
        dynamic_operating_envelopes: components['schemas']['DynamicOperatingEnvelopes'];
      };
      GeneralFieldsEnrollment: {
        der_id: string;
        service_provider_id: number;
        program_id: number;
      };
      CreateUpdateEnrollment: {
        general_fields: components['schemas']['GeneralFieldsEnrollment'];
        demand_response?: components['schemas']['DemandResponse'];
        dynamic_operating_envelopes?: components['schemas']['DynamicOperatingEnvelopes'];
      };
      EnrollmentCRUDResponse: {
        id?: string;
        message?: string;
        /** @enum {string} */
        status: 'CREATED' | 'NOT_CREATED' | 'RETRIEVED' | 'UPDATED' | 'NOT_UPDATED' | 'DELETED' | 'NOT_DELETED';
        data?: Record<string, never>;
      };
      EnrollmentRequestsUpload: {
        /** Format: byte */
        file: string;
      };
      ContractError: {
        code?: number;
        errors?: Record<string, never>;
        message?: string;
        status?: string;
      };
      Contract: {
        id: number;
        /** @enum {string} */
        contract_type: 'ENROLLMENT_CONTRACT';
        dynamic_operating_envelopes?: components['schemas']['DynamicOperatingEnvelopes'];
        der_id: string;
        program_id: number;
        demand_response?: components['schemas']['DemandResponse'];
        service_provider_id: number;
        enrollment_request_id: number;
        /** @enum {string} */
        contract_status: 'ACCEPTED' | 'ACTIVE' | 'EXPIRED' | 'USER_CANCELLED' | 'SYSTEM_CANCELLED';
      };
      UpdateContract: {
        /** @enum {string} */
        contract_type: 'ENROLLMENT_CONTRACT';
        dynamic_operating_envelopes?: components['schemas']['DynamicOperatingEnvelopes'];
        der_id: string;
        program_id: number;
        demand_response?: components['schemas']['DemandResponse'];
        service_provider_id: number;
        enrollment_request_id: number;
        /** @enum {string} */
        contract_status: 'ACCEPTED' | 'ACTIVE' | 'EXPIRED' | 'USER_CANCELLED' | 'SYSTEM_CANCELLED';
      };
      NumberViolation: {
        violation: boolean;
        number: number;
      };
      EventDuration: {
        YEAR?: components['schemas']['NumberViolation'];
        WEEK?: components['schemas']['NumberViolation'];
        MONTH?: components['schemas']['NumberViolation'];
        DAY?: components['schemas']['NumberViolation'];
        PROGRAM_DURATION?: components['schemas']['NumberViolation'];
      };
      ProcessedConstraints: {
        max_number_of_events_per_timeperiod?: components['schemas']['EventDuration'];
        max_total_energy_per_timeperiod?: components['schemas']['EventDuration'];
        cumulative_event_duration?: components['schemas']['EventDuration'];
        opt_outs?: components['schemas']['EventDuration'];
      };
      DersNoSPResponse: {
        name?: string;
        der_id: string;
        /** @enum {string} */
        nameplate_rating_unit?:
          | 'kW'
          | 'kVAr'
          | 'MW'
          | 'MVAr'
          | 'V'
          | 'A'
          | 'PF'
          | 'kWh'
          | 'MWh'
          | 'kVArh'
          | 'MVArh'
          | 'load_factor'
          | 'F';
        /** @enum {string} */
        der_type?: 'DR' | 'BESS' | 'EV_CHRG_STN' | 'PV' | 'WIND_FARM' | 'SYNC_GEN';
        /** @enum {string} */
        resource_category?: 'CNI' | 'GENERIC' | 'RES' | 'UTIL' | 'VPP';
        nameplate_rating?: number;
      };
    };
    responses: {
      /** @description Default error response */
      DEFAULT_ERROR: {
        content: {
          'application/json': components['schemas']['Error'];
        };
      };
      /** @description Unprocessable Entity */
      UNPROCESSABLE_ENTITY: {
        content: {
          'application/json': components['schemas']['Error'];
        };
      };
    };
    parameters: never;
    requestBodies: never;
    headers: never;
    pathItems: never;
  }