const express = require('express');
const cors = require('cors');
const app = express();
const port = 3001;

var corsOptions = {
    origin: 'http://localhost:4200',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
  }

app.use(cors());

app.get('/', cors(corsOptions), (req, res) => {
    res.send('hello world');
})

app.get('/api', cors(corsOptions), (req, res) => {
    
})


app.get('/api/health-check', cors(corsOptions), (req, res) => {
    res.sendStatus(200);
})



app.get('/api/program/', cors(corsOptions), (req, res) => {
    res.json({
        "pagination_start": 0,
        "pagination_end": 100,
        "count": 100,
        "results": [
            {
                "id": "1",
                "name": "Program 1",
                "created_at": "2022-01-01T00:00:00.000Z",
                "created_by": "Rolls Canhardly",
                "status": "DRAFT",
                "start_date": "2022-01-01T00:00:00.000Z",
                "end_date": "2022-01-01T00:00:00.000Z",
                "program_type": "GENERIC"
            },
            {
                "id": "2",
                "name": "Program 2",
                "created_at": "2022-01-01T00:00:00.000Z",
                "created_by": "Amanda Huginkiss",
                "status": "DRAFT",
                "start_date": "2022-01-01T00:00:00.000Z",
                "end_date": "2022-01-01T00:00:00.000Z",
                "program_type": "GENERIC"
            },
        ]
    })
    
})

app.post('/api/program/', cors(corsOptions), (req, res) => {
    
})

app.get('/api/program/:id', cors(corsOptions), (req, res) => {
    res.json({
        "name": "Program 1",
        "dispatch_constraints": [
            
        ],
        "resource_eligibility_criteria": [
            
        ],
        "limit_type": 'REAL_POWER',
        "dispatch_type": "DER_GATEWAY",
        "control_type": "EXPORT_LIMIT_ONLY",
        "availability_type": 'ALWAYS_AVAILABLE',
        "avail_operating_months": [
            
        ],
        "check_der_eligibility": true,
        "program_priority": "P1",
        "dispatch_notifications": [
            
        ],
        "schedule_time_horizon_number": 1,
        "holiday_exclusions": [ 
        ],
        "avail_service_windows": [
        ],
        "demand_management_constraints": [
            
        ],
        "track_event": true,
        "status": "DRAFT",
        "start_date": "2022-01-01T00:00:00.000Z",
        "control_options": "OP_MOD_CONNECT",
        "program_type": "GENERIC",
                                


    })
})

app.get('/api/program/:id/holiday-exclusions', cors(corsOptions), (req, res) => {
    
})

app.put('/api/program/:id/archive', cors(corsOptions), (req, res) => {
    
})

app.get('/api/program/:id/available_ders', cors(corsOptions), (req, res) => {
    
})

app.get('/api/program/:id/enrollments', cors(corsOptions), (req, res) => {
    
})


app.get('/api/serviceprovider/', cors(corsOptions), (req, res) => {

    res.json({
        "pagination_start": 0,
        "pagination_end": 100,
        "count": 100,
        "results": [
            {
                "service_provider_type": "AGGREGATOR",
                "name": "Service Provider Name",
                "notification_contact": {"email_address": "bob@bob.com", "phone_number": "217-258-6961"},
                "status": "ACTIVE",
                "address": {"city": "Walla Walla", "state": "WA", "apt_unit": "1", "country": "United States", "street": "Anywhere Street", "zip_code": "28228"},
                "ders": [],
                "primary_contact": {"email_address": "steve@steve.com", "phone_number": "555-555-5555"}
            },
            {
                "service_provider_type": "AGGREGATOR",
                "name": "Service Provider Name",
                "notification_contact": {"email_address": "bob@bob.com", "phone_number": "217-258-6961"},
                "status": "ACTIVE",
                "address": {"city": "Walla Walla", "state": "WA", "apt_unit": "1", "country": "United States", "street": "Anywhere Street", "zip_code": "28228"},
                "ders": [],
                "primary_contact": {"email_address": "steve@steve.com", "phone_number": "555-555-5555"}
            },
        ]
   
 })
})

app.post('/api/serviceprovider/', cors(corsOptions), (req, res) => {

    
})

app.get('/api/serviceprovider/:id', cors(corsOptions),  (req, res) => {
    
})


app.post('/api/serviceprovider/upload', cors(corsOptions),  (req, res) => {
    
})

app.get('/api/serviceprovider/download_service_provider_template', cors(corsOptions),  (req, res) => {
    
})

app.post('/api/serviceprovider/:id/upload', cors(corsOptions), (req, res) => {
    
})

app.get('/api/der/non_associated_ders', cors(corsOptions), (req, res) => {
    res.json({
        "name": "Der 1",
        "der_id": "1",
        "nameplate_rating_unit": "kW",
        "der_type": "SYNC_GEN",
        "resource_category": "GENERIC",
        "nameplate_rating": 1
    })
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
})